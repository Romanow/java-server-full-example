#### Сброка проекта
Сборка выполняется в jar файл с embedded tomcat-контейнером.

`gradle clean build`

#### Запуск проекта

`gradle clean bootRun`

Запуск происходит как standalone приложение.
После запуска можно зайти в браузере `http://localhost:8080/management/health` - health-чек.  