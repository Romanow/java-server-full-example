package ru.romanow.server.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.romanow.server.domain.Customer;
import ru.romanow.server.repository.CustomerRepository;

import javax.persistence.EntityNotFoundException;

/**
 * Created by romanow on 31.10.16
 */
@Service
public class CustomerServiceImpl
        implements CustomerService {

    @Autowired
    private CustomerRepository customerRepository;

    @Override
    @Transactional(readOnly = true)
    public Customer getById(Integer id) {
        Customer customer = customerRepository.findOne(id);
        if (customer == null) {
            throw new EntityNotFoundException("Customer '{" + id + "}' not found");
        }
        return customer;
    }

    @Override
    @Transactional
    public Customer save(Customer customer) {
        return customerRepository.save(customer);
    }
}
