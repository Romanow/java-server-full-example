package ru.romanow.server.service;

import ru.romanow.server.domain.Customer;

/**
 * Created by romanow on 31.10.16
 */
public interface CustomerService {
    Customer getById(Integer id);

    Customer save(Customer customer);
}
