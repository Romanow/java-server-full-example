package ru.romanow.server.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.romanow.server.domain.Customer;
import ru.romanow.server.domain.Order;
import ru.romanow.server.repository.OrderRepository;
import ru.romanow.server.web.model.CustomerRequest;
import ru.romanow.server.web.model.OrderRequest;

import javax.persistence.EntityNotFoundException;
import java.util.List;

import static org.springframework.data.jpa.domain.AbstractPersistable_.id;

/**
 * Created by romanow on 31.10.16
 */
@Service
public class OrderServiceImpl
        implements OrderService {

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private CustomerService customerService;

    @Override
    @Transactional(readOnly = true)
    public Order getById(Integer id) {
        Order order = orderRepository.findOne(id);
        if (order == null) {
            throw new EntityNotFoundException("Order '{" + id + "}' not found");
        }
        return order;
    }

    @Override
    @Transactional(readOnly = true)
    public List<Order> findAll() {
        return orderRepository.findAll();
    }

    @Override
    @Transactional
    public Order save(OrderRequest orderRequest) {
        Order order = new Order()
                .setSum(orderRequest.getSum())
                .setDiscount(orderRequest.getDiscount());

        return orderRepository.save(order);
    }

    @Override
    @Transactional
    public Order update(Integer id, OrderRequest orderRequest) {
        Order order = orderRepository.findOne(id);
        if (order == null) {
            throw new EntityNotFoundException("Order '{" + id + ")}' not found");
        }
        order.setSum(orderRequest.getSum() != null ? orderRequest.getSum() : order.getSum());
        order.setDiscount(orderRequest.getDiscount() != null ? orderRequest.getDiscount() : order.getDiscount());

        return orderRepository.save(order);
    }

    @Override
    @Transactional
    public void delete(Integer id) {
        orderRepository.delete(id);
    }

    @Override
    @Transactional
    public Integer addCustomerToOrder(Integer orderId, CustomerRequest customerRequest) {
        Order order = orderRepository.findOne(orderId);
        if (order == null) {
            throw new EntityNotFoundException("Order '{" + id + ")}' not found");
        }
        Customer customer = new Customer()
                .setLogin(customerRequest.getLogin());

        customer = customerService.save(customer);
        order.setCustomer(customer);
        orderRepository.save(order);
        return customer.getId();
    }
}
