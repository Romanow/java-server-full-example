package ru.romanow.server.service;

import ru.romanow.server.domain.Order;
import ru.romanow.server.web.model.CustomerRequest;
import ru.romanow.server.web.model.OrderRequest;

import java.util.List;

/**
 * Created by romanow on 31.10.16
 */
public interface OrderService {

    Order getById(Integer id);

    List<Order> findAll();

    Order save(OrderRequest orderRequest);

    Order update(Integer id, OrderRequest orderRequest);

    void delete(Integer id);

    Integer addCustomerToOrder(Integer orderId, CustomerRequest customerRequest);
}
