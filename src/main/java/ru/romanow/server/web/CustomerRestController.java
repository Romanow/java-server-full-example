package ru.romanow.server.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ru.romanow.server.service.CustomerService;
import ru.romanow.server.web.model.CustomerResponse;

/**
 * Created by romanow on 31.10.16
 */
@RestController
@RequestMapping("/customer")
public class CustomerRestController {

    @Autowired
    private CustomerService customerService;

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public CustomerResponse getCustomer(@PathVariable Integer id) {
        return new CustomerResponse(customerService.getById(id));
    }
}
