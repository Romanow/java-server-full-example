package ru.romanow.server.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import ru.romanow.server.domain.Order;
import ru.romanow.server.service.OrderService;
import ru.romanow.server.web.model.CustomerRequest;
import ru.romanow.server.web.model.OrderRequest;
import ru.romanow.server.web.model.OrderResponse;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by romanow on 31.10.16.
 */
@RestController
@RequestMapping("/order")
public class OrderRestController {

    @Autowired
    private OrderService orderService;

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public OrderResponse getOrder(@PathVariable Integer id) {
        return new OrderResponse(orderService.getById(id));
    }

    @RequestMapping(method = RequestMethod.GET)
    public List<OrderResponse> findAll() {
        return orderService.findAll()
                           .stream()
                           .map(OrderResponse::new)
                           .collect(Collectors.toList());
    }

    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(method = RequestMethod.POST)
    public void createOrder(@RequestBody OrderRequest orderRequest, HttpServletResponse response) {
        Order order = orderService.save(orderRequest);
        response.addHeader(HttpHeaders.LOCATION, "/order/" + order.getId());
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PATCH)
    public OrderResponse updateOrder(@PathVariable Integer id, @RequestBody OrderRequest order) {
        return new OrderResponse(orderService.update(id, order));
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void deleteOrder(@PathVariable Integer id) {
        orderService.delete(id);
    }

    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(value = "/{orderId}/customer", method = RequestMethod.POST)
    public void addCustomerToOrder(@PathVariable Integer orderId, @RequestBody CustomerRequest customerRequest, HttpServletResponse response) {
        Integer customerId = orderService.addCustomerToOrder(orderId, customerRequest);
        response.addHeader(HttpHeaders.LOCATION, "/customer/" + customerId);
    }
}
