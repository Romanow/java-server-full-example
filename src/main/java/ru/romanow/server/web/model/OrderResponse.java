package ru.romanow.server.web.model;

import com.google.common.base.MoreObjects;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.romanow.server.domain.Order;

/**
 * Created by romanow on 31.10.16
 */
@Data
@NoArgsConstructor
public class OrderResponse {
    private Integer sum;
    private Integer discount;
    private CustomerResponse customer;

    public OrderResponse(Order order) {
        this.sum = order.getSum();
        this.discount = order.getDiscount();
        if (order.getCustomer() != null) {
            this.customer = new CustomerResponse(order.getCustomer());
        }
    }
}
