package ru.romanow.server.web.model;

import com.google.common.base.MoreObjects;
import lombok.Data;

/**
 * Created by romanow on 31.10.16
 */
@Data
public class OrderRequest {
    private Integer sum;
    private Integer discount;
}