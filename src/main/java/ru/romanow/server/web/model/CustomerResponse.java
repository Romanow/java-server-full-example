package ru.romanow.server.web.model;

import com.google.common.base.MoreObjects;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.romanow.server.domain.Customer;

/**
 * Created by romanow on 31.10.16
 */
@Data
@NoArgsConstructor
public class CustomerResponse {
    private String login;

    public CustomerResponse(Customer customer) {
        this.login = customer.getLogin();
    }
}
