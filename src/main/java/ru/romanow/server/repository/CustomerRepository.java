package ru.romanow.server.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.romanow.server.domain.Customer;

/**
 * Created by romanow on 31.10.16
 */
@Repository
public interface CustomerRepository
        extends CrudRepository<Customer, Integer> {}
