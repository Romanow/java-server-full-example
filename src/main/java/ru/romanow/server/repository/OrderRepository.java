package ru.romanow.server.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.romanow.server.domain.Order;

/**
 * Created by romanow on 31.10.16
 */
@Repository
public interface OrderRepository
        extends JpaRepository<Order, Integer> {}
